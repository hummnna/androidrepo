package com.example.user.zadanie01;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Created by User on 2016-03-23.
 */
public class Zadanie1 extends AppCompatActivity {

    RatingBar gwiazdy1do5;
    RatingBar gwiazdy5do10;
    TextView wynikSekcja1;

    TextView wynikSekcja2;
    Button buttonSekcja2;

    EditText wynikSekcja3;

    Button buttonKoniec;
    TextView wynikAP;
    CheckBox checkBox;


    Float liczbaSekcja1 ;
    Integer liczbaKlikniec = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zadanie1);

        gwiazdy1do5 = (RatingBar) findViewById(R.id.gwiazdy1do5);
        gwiazdy5do10 = (RatingBar) findViewById(R.id.gwiazdy5do10);
        wynikSekcja1 = (TextView) findViewById(R.id.wynikSekcja1);
        buttonSekcja2 = (Button) findViewById(R.id.buttonSekcja2);
        wynikSekcja2 = (TextView) findViewById(R.id.wynikSekcja2);
        wynikSekcja3 = (EditText) findViewById(R.id.wynikSekcja3);
        buttonKoniec = (Button) findViewById(R.id.buttonKoniec);
        wynikAP = (TextView) findViewById(R.id.wynikAP);
        checkBox = (CheckBox) findViewById(R.id.checkBox);


        LayerDrawable stars = (LayerDrawable) gwiazdy1do5.getProgressDrawable();
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(2).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        LayerDrawable stars1 = (LayerDrawable) gwiazdy5do10.getProgressDrawable();
        stars1.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars1.getDrawable(2).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_OUT);


        gwiazdy1do5.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar gwiazdy1do5, float rating, boolean fromUser) {
                float liczba = gwiazdy1do5.getRating() + gwiazdy5do10.getRating();
                liczbaSekcja1 = liczba;
                String wynik = String.valueOf(liczba);
                wynikSekcja1.setText(wynik);

            }
        });
        gwiazdy5do10.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar gwiazdy5do10, float rating, boolean fromUser) {
                float liczba = gwiazdy1do5.getRating() + gwiazdy5do10.getRating();
                liczbaSekcja1 = liczba;
                String wynik = String.valueOf(liczba);
                wynikSekcja1.setText(wynik);

            }
        });

        buttonSekcja2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                liczbaKlikniec += 1;
                String wynik = Integer.toString(liczbaKlikniec);
                wynikSekcja2.setText(wynik);
            }
        });


        buttonKoniec.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Editable wynikSekcja3STR = wynikSekcja3.getText();

                if (liczbaSekcja1 == null) {
                    liczbaSekcja1 = Float.valueOf(0);
                }
                if (liczbaKlikniec == null) {
                    liczbaKlikniec = 0;
                }
                boolean flaga = checkBox.isChecked();
                if(flaga){
                    Float wynikKoncowyInt = liczbaSekcja1 * liczbaKlikniec * Integer.valueOf(String.valueOf(wynikSekcja3STR));
                    wynikAP.setText(String.valueOf(wynikKoncowyInt));
                } else {
                    Float wynikKoncowyInt = liczbaSekcja1 + liczbaKlikniec + Integer.valueOf(String.valueOf(wynikSekcja3STR));
                    wynikAP.setText(String.valueOf(wynikKoncowyInt));
                }


            }


        });

    }
}

