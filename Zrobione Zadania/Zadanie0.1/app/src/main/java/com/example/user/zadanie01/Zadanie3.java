package com.example.user.zadanie01;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Zadanie3 extends AppCompatActivity{
    ListView listView;
    Button buttonDodaj;
    EditText textEditImie;
    EditText textEditNazwisko;
    Context context;
    String[] values = new String[]{"Jan Nowak", "Paweł Sęk"};
    ArrayList<String> list = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zadanie3);
        context = getApplicationContext();
        listView = (ListView) findViewById(R.id.listView);
        textEditImie  = (EditText) findViewById(R.id.textEditImie);
        textEditNazwisko = (EditText) findViewById(R.id.textEditNazwisko);
        buttonDodaj = (Button) findViewById(R.id.buttonDodaj);









        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Click ListItem Number " + position, Toast.LENGTH_LONG)
                        .show();
            }
        });


        buttonDodaj.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String imieNazwiskoString = String.valueOf(textEditImie.getText()) + String.valueOf(textEditNazwisko.getText());
                list.add(imieNazwiskoString);
                final StableArrayAdapter adapter = new StableArrayAdapter(context,
                        android.R.layout.simple_list_item_multiple_choice, list);
                listView.setAdapter(adapter);


            }


        });


    }

    @Override
    public void onResume() {
        super.onResume();
        listView = (ListView) findViewById(R.id.listView);


        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_multiple_choice, list);
        listView.setAdapter(adapter);

    }
}


class StableArrayAdapter extends ArrayAdapter<String> {

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

    public StableArrayAdapter(Context context, int textViewResourceId,List<String> objects) {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

}


