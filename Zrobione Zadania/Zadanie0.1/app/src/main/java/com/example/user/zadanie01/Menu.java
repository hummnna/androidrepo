package com.example.user.zadanie01;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Menu extends AppCompatActivity {
    Button buttonZadanie1;
    Button buttonZadanie2;
    Button buttonZadanie3;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        context = getApplicationContext();
        buttonZadanie1 = (Button) findViewById(R.id.buttonZadanie1);
        buttonZadanie2 = (Button) findViewById(R.id.buttonZadanie2);
        buttonZadanie3 = (Button) findViewById(R.id.buttonZadanie3);

        buttonZadanie1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, Zadanie1.class);
                startActivity(intent);


            }
        });


        buttonZadanie2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, Zadanie2.class);
                startActivity(intent);


            }
        });


        buttonZadanie3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, Zadanie3.class);
                startActivity(intent);


            }
        });
    }
}
