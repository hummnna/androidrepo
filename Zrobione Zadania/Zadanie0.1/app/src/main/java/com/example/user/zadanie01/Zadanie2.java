package com.example.user.zadanie01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Created by User on 2016-03-23.
 */
public class Zadanie2 extends AppCompatActivity {
    Button dalej;
    EditText editImie;
    EditText editData;
    Context context;
    ImageButton buttonAcce;
    Boolean flaga = true;
    int wartosc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zadanie2);
        context = getApplicationContext();

        dalej = (Button) findViewById(R.id.dalej);
        editImie = (EditText)findViewById(R.id.editImie);
        editData = (EditText)findViewById(R.id.editData);
        buttonAcce = (ImageButton)findViewById(R.id.buttonAcce);


        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.int1), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.int1), 0);
        editor.commit();

        dalej.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, Zadanie2act1.class);
                String imie = editImie.getText().toString();
                String data = editData.getText().toString();
                intent.putExtra("textImie", imie);
                intent.putExtra("textData", data);
                startActivity(intent);
                flaga=!flaga;

            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.int1), Context.MODE_PRIVATE);
        wartosc = sharedPref.getInt(getString(R.string.int1), 0);
        flaga = !(wartosc == 1);
        if(flaga){
            dalej.setVisibility(View.VISIBLE);
            buttonAcce.setVisibility(View.INVISIBLE);

        } else {
            dalej.setVisibility(View.INVISIBLE);
            buttonAcce.setVisibility(View.VISIBLE);
            wartosc=0;
        }

    }



}
