package com.example.user.zadanie01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by User on 2016-03-23.
 */
public class Zadanie2act1  extends AppCompatActivity {

    TextView textImie;
    TextView textData;
    Button buttonZatw;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zadanie2activity1);
        textImie = (TextView) findViewById(R.id.textImie);
        textData = (TextView) findViewById(R.id.textData);
        buttonZatw = (Button) findViewById(R.id.buttonZatw);

        Bundle bundle = getIntent().getExtras();
        String textImieString = bundle.getString("textImie");
        String textDataString = bundle.getString("textData");

        textImie.setText(textImieString);
        textData.setText(textDataString);

        context = getApplicationContext();


        buttonZatw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (textImie.length() > 0 && textData.length() > 0) {
                    buttonZatw.setText("accepted");
                    SharedPreferences sharedPref = context.getSharedPreferences(getString(R.string.int1), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt(getString(R.string.int1), 1);
                    editor.commit();
                } else {
                    buttonZatw.setText("nie moga byc puste");
                }

            }
        });

    }

}
