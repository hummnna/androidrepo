package com.example.user.zaliczenie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by User on 2016-06-21.
 */
public class Another extends Activity {
    TextView textView5;
    EditText editText2;
    TextView textView9;
    Context context;
    Button button2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.another);

        textView5 = (TextView) findViewById(R.id.textView5);
        editText2 = (EditText) findViewById(R.id.editText2);
        textView9 = (TextView) findViewById(R.id.textView9);
        button2 = (Button) findViewById(R.id.button2);

        Bundle bundle = getIntent().getExtras();
        String textLiczba = bundle.getString("textPobranyLiczba");
        textView5.setText(textLiczba);
        Log.d("OnCreat drugiekran","niewchodze tu !!!!!!!!!!!!!! "+bundle);
        ////////////////////////////////////////////////////
        button2.setVisibility(View.GONE);
        ////////////////////////////////////////////////////
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String liczbaString = textView9.getText().toString();
                Intent backActivity = new Intent(getApplicationContext(), Main.class);
                backActivity.putExtra("zwroconaLiczba", liczbaString);
                startActivity(backActivity);
            }
        });

        editText2.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    int liczba = Integer.parseInt(editText2.getText().toString()) * Integer.parseInt(textView5.getText().toString());
                    textView9.setText(""+liczba);
                    button2.setVisibility(View.VISIBLE);
                    return true;
                }
                return false;
            }
        });

    }


}
