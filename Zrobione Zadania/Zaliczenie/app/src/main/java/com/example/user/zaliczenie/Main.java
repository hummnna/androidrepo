package com.example.user.zaliczenie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main extends AppCompatActivity {
    RelativeLayout zadanie1;
    RelativeLayout zadanie2;
    RelativeLayout zadanie3;

    RadioGroup radioGroup;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;

    TextView Zadanie1textView;
    Button Zadanie1Minus;
    Button Zadanie1Plus;
    Switch Zadanie1Switch;

    TextView Zadanie2textView4;
    TextView Zadanie2textView5;
    EditText Zadanie2editText2;
    TextView Zadanie2textView6;
    Button Zadanie2button2;

    ListView Zadanie3listView;
    Button Zadanie3Button;
    EditText Zadanie3TextEdit;

    Context context;
    int wartosc;
    ArrayList<String> list = new ArrayList<String>();
    String[] values = new String[] { "Polska","Niemcy","Czechy","Francja","Pakistan","Iran","Gruzja","Horwacja","Chiny","Włochy","Hiszpania","Czeczennia","Kazachstan","Czile" };


    Boolean flagaZadanie1 = true;
    /////////////////////////////////////////////////////////////
    //                      onCreate
    /////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        zadanie1 = (RelativeLayout) findViewById(R.id.Zadanie1);
        zadanie2 = (RelativeLayout) findViewById(R.id.Zadanie2_1);
        zadanie3 = (RelativeLayout) findViewById(R.id.Zadanie3);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
        radioButton3 = (RadioButton) findViewById(R.id.radioButton3);

        Zadanie1textView = (TextView) findViewById(R.id.Zadanie1textView);
        Zadanie1Minus = (Button)  findViewById(R.id.Zadanie1Minus);
        Zadanie1Plus = (Button)  findViewById(R.id.Zadanie1Plus);
        Zadanie1Switch = (Switch)  findViewById(R.id.Zadanie1Switch);

        Zadanie2textView4 = (TextView) findViewById(R.id.Zadanie2textView4);
        Zadanie2textView5 = (TextView) findViewById(R.id.Zadanie2textView5);
        Zadanie2editText2 = (EditText) findViewById(R.id.Zadanie2editText2);
        Zadanie2textView6 = (TextView) findViewById(R.id.Zadanie2textView6);
        Zadanie2button2   = (Button) findViewById(R.id.Zadanie2button2);

        Zadanie3listView = (ListView) findViewById(R.id.Zadanie3listView);



        /////////////////////////////////////////////////////////////
        zadanie1.setVisibility(View.GONE);
        zadanie2.setVisibility(View.GONE);
        zadanie3.setVisibility(View.GONE);
        Zadanie1textView.setText("0");
        /*
        SharedPreferences sharedPref = context.getSharedPreferences("0", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("0", 0);
        editor.commit();
        */
        /////////////////////////////////////////////////////////////
        radioButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                widocznoscZadan();
            }
        });
        radioButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                widocznoscZadan();
            }
        });
        radioButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                widocznoscZadan();
            }
        });

        Zadanie1Plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ustawZadanie1("d", flagaZadanie1);
            }
        });
        Zadanie1Minus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ustawZadanie1("o", flagaZadanie1);
            }
        });
        Zadanie1Switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
            if(isChecked){
                flagaZadanie1 = true;
                Zadanie1Plus.setText("+");
                Zadanie1Minus.setText("-");
            }else{
                flagaZadanie1 = false;
                Zadanie1Plus.setText("-");
                Zadanie1Minus.setText("+");

            }

            }
        });

        Zadanie2button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String liczba = Zadanie2editText2.getText().toString();
                Intent startAnotherActivity = new Intent(getApplicationContext(), Another.class);
                startAnotherActivity.putExtra("textPobranyLiczba", liczba);
                //startActivity(startAnotherActivity);
                startActivityForResult(startAnotherActivity,1);
            }
        });





    }
    /////////////////////////////////////////////////////////////
    //                      Resume
    /////////////////////////////////////////////////////////////
    @Override
    public void onResume(){
        super.onResume();
       /* SharedPreferences sharedPref = context.getSharedPreferences("0", Context.MODE_PRIVATE);
        wartosc = sharedPref.getInt("0", 0);
       */
        Bundle bundle = getIntent().getExtras();
        Log.d("Resume","niewchodze tu !!!!!!!!!!!!!! "+bundle);
        if(bundle != null) {
            zadanie1.setVisibility(View.GONE);
            zadanie2.setVisibility(View.VISIBLE);
            zadanie3.setVisibility(View.GONE);
            Log.d("Resume","wchodze tu !!!!!!!!!!!!!!");
            String textLiczbaZwrocona = bundle.getString("zwroconaLiczba");
            Zadanie2textView6.setText(textLiczbaZwrocona);
        }

        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_single_choice, list);
        Zadanie3listView.setAdapter(adapter);

        Zadanie3listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                view.animate().setDuration(2000).alpha(0)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                list.remove(item);
                                adapter.notifyDataSetChanged();
                                view.setAlpha(1);
                            }
                        });
            }
        });
    }

    /////////////////////////////////////////////////////////////
    //                      Funkcje
    /////////////////////////////////////////////////////////////
    void widocznoscZadan() {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        Log.d("ZAdanie1: ", "" + selectedId + "");
        if (selectedId != 0) {
            radioButton1 = (RadioButton) findViewById(selectedId);
            String nazwaKoloru = (String) radioButton1.getText();
            Log.d("ZAdanie1: ", "" + nazwaKoloru + "");
            switch (nazwaKoloru) {
                case "Zadanie 1":
                    zadanie1.setVisibility(View.VISIBLE);
                    zadanie2.setVisibility(View.GONE);
                    zadanie3.setVisibility(View.GONE);
                    break;
                case "Zadanie 2":
                    zadanie1.setVisibility(View.GONE);
                    zadanie2.setVisibility(View.VISIBLE);
                    zadanie3.setVisibility(View.GONE);
                    break;
                case "Zadanie 3":
                    zadanie1.setVisibility(View.GONE);
                    zadanie2.setVisibility(View.GONE);
                    zadanie3.setVisibility(View.VISIBLE);
                    break;
            }

        }
    }

    void ustawZadanie1 (String s, Boolean t){
        int liczbaZKontrol = 0;
        if(t) {
            if (s == "d") {
                liczbaZKontrol = Integer.parseInt(Zadanie1textView.getText().toString()) + 1;
            } else if (s == "o") {
                liczbaZKontrol = Integer.parseInt(Zadanie1textView.getText().toString()) - 1;
            }
        } else {
            if (s == "d") {
                liczbaZKontrol = Integer.parseInt(Zadanie1textView.getText().toString()) - 1;
            } else if (s == "o") {
                liczbaZKontrol = Integer.parseInt(Zadanie1textView.getText().toString()) + 1;
            }
        }
        Zadanie1textView.setText(""+liczbaZKontrol+"");
    }


}


class StableArrayAdapter extends ArrayAdapter<String> {

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

    public StableArrayAdapter(Context context, int textViewResourceId,List<String> objects) {
        super(context, textViewResourceId, objects);
        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

}
