package com.example.user.aplikacjatreningowa;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class Menu extends AppCompatActivity {
    private static final String PREFERENCES_NAME = "myPreferences";
    SharedPreferences data;
    Boolean firstStartFlagPref = true;
    Integer idAplicationPref;

    EditText username;
    String userNameText;

    private TodoDbAdapter todoDbAdapter;
    String taskDescription = "baba";
    private Cursor todoCursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button menuHistory = (Button) findViewById(R.id.menuHistory);
        Button menuTraining = (Button) findViewById(R.id.menuTraining);
        Button menuAchievements = (Button) findViewById(R.id.menuAchievements);
        Button menuInformation = (Button) findViewById(R.id.menuInformation);
        todoDbAdapter = new TodoDbAdapter(getApplicationContext());
        todoDbAdapter.open();
        todoDbAdapter.insertTodo(taskDescription);
        todoCursor = todoDbAdapter.getAllTodos();
        todoCursor.moveToLast();
        int ilosc = todoCursor.getPosition();
        todoCursor.moveToFirst();
        for (int i=0; i<ilosc;i++) {
            String wynik = todoCursor.getString(0);
            String wynik1 = todoCursor.getString(1);
            String wynik2 = todoCursor.getString(2);
            //todoCursor = getAllEntriesFromDb();
            Log.d("POKAPOKAPOKAPOKA", wynik);
            Log.d("POKAPOKAPOKAPOKA", wynik1);
            Log.d("POKAPOKAPOKAPOKA", wynik2);
            todoCursor.moveToNext();
        }
        todoDbAdapter.close();



        data = getSharedPreferences(PREFERENCES_NAME, Activity.MODE_PRIVATE);
        firstStartFlagPref = data.getBoolean("firstStartFlagPref", true);
        idAplicationPref = data.getInt("idAplicationPref",1);
        if(firstStartFlagPref){
            todoDbAdapter = new TodoDbAdapter(getApplicationContext());
            todoDbAdapter.open();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = getLayoutInflater();
            alertDialogBuilder.setView(inflater.inflate(R.layout.dialog, null));
            alertDialogBuilder.setMessage("Tworzenie nowego użytkownika");
            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    userNameText = String.valueOf(username.getText());
                    new WebServiceHandler()
                            .execute("http://hummnna.cba.pl/pierwszeUruchomienie.php");
                    Toast.makeText(Menu.this,"Witaj "+userNameText+", utworzono dla ciebie nowe konto", Toast.LENGTH_LONG).show();
                    SharedPreferences.Editor preferencesEditor = data.edit();
                    preferencesEditor.putBoolean("firstStartFlagPref", false);
                    preferencesEditor.putString("nickPref", userNameText);
                    preferencesEditor.commit();
                }
            });
            alertDialogBuilder.setNegativeButton("Anuluj",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            username = (EditText) alertDialog.findViewById(R.id.username);
        }

        menuHistory.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startAnotherActivity = new Intent(getApplicationContext(), History.class);
                startActivity(startAnotherActivity);
            }
        });
        menuTraining.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startAnotherActivity = new Intent(getApplicationContext(), Training.class);
                startActivity(startAnotherActivity);
            }
        });
        menuAchievements.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startAnotherActivity = new Intent(getApplicationContext(), Achievements.class);
                startActivity(startAnotherActivity);
            }
        });
        menuInformation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent startAnotherActivity = new Intent(getApplicationContext(), Information.class);
                startActivity(startAnotherActivity);
            }
        });



    }


    private Cursor getAllEntriesFromDb() {
        todoCursor = todoDbAdapter.getAllTodos();
        if(todoCursor != null) {
            startManagingCursor(todoCursor);
            todoCursor.moveToFirst();
        }
        return todoCursor;
    }


    private class WebServiceHandler extends AsyncTask<String, Void, String> {
        // okienko dialogowe, które każe użytkownikowi czekać
        private ProgressDialog dialog = new ProgressDialog(Menu.this);

        // metoda wykonywana jest zaraz przed główną operacją (doInBackground())
        // mamy w niej dostęp do elementów UI
        @Override
        protected void onPreExecute() {
            // wyświetlamy okienko dialogowe każące czekać
            dialog.setMessage("Czekaj...");
            dialog.show();
        }

        // główna operacja, która wykona się w osobnym wątku
        // nie ma w niej dostępu do elementów UI
        @Override
        protected String doInBackground(String... urls) {

            try {
                // zakładamy, że jest tylko jeden URL
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                // zezwolenie na wysyłanie danych
                connection.setDoOutput(true);
                // ustawienie typu wysyłanych danych
                connection.setRequestProperty("Content-Type",
                        "application/json");
                // ustawienie metody
                connection.setRequestMethod("POST");

                // stworzenie obiektu do wysłania
                JSONObject data = new JSONObject();
                //data.put("name", idAplication);
                data.put("idAplication", idAplicationPref);
                data.put("userNameText", userNameText);

                // wysłanie obiektu
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(connection.getOutputStream(),
                                "UTF-8"));
                writer.write(data.toString());
                writer.close();

                // sprawdzenie kodu odpowiedzi, 200 = OK
                if (connection.getResponseCode() != 200) {
                    throw new Exception("Bad Request");
                }


                // pobranie danych do InputStream
                InputStream in = new BufferedInputStream(
                        connection.getInputStream());

                // konwersja InputStream na String
                // wynik będzie przekazany do metody onPostExecute()
                return streamToString(in);

            } catch (Exception e) {
                // obsłuż wyjątek
                Log.d(Menu.class.getSimpleName(), e.toString());
                return null;
            }

        }

        // metoda wykonuje się po zakończeniu metody głównej,
        // której wynik będzie przekazany;
        // w tej metodzie mamy dostęp do UI
        @Override
        protected void onPostExecute(String result) {

            // chowamy okno dialogowe
            dialog.dismiss();

            try {
                // reprezentacja obiektu JSON w Javie
                JSONObject json = new JSONObject(result);
                idAplicationPref = Integer.parseInt(json.optString("id"));
                Log.d("Zwrocona wartosc to: ",""+idAplicationPref);
           } catch (Exception e) {
                // obsłuż wyjątek
                Log.d(Menu.class.getSimpleName(), e.toString());
            }
        }
    }

    // konwersja z InputStream do String
    public static String streamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;

        try {

            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            reader.close();

        } catch (IOException e) {
            // obsłuż wyjątek
            Log.d(Menu.class.getSimpleName(), e.toString());
        }

        return stringBuilder.toString();
    }


}

