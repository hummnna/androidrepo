package com.example.user.aplikacjatreningowa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.util.ArrayList;

public class Training extends AppCompatActivity {
    Object[][] buttons = new Object[][]{{R.id.imageButton, R.id.imageButton2, R.id.imageButton3, R.id.imageButton4, R.id.imageButton5, R.id.imageButton6, R.id.imageButton7, R.id.imageButton8, R.id.imageButton9, R.id.imageButton10, R.id.imageButton11, R.id.imageButton12, R.id.imageButton13, R.id.imageButton14, R.id.imageButton15, R.id.imageButton16, R.id.imageButton17, R.id.imageButton18, R.id.imageButton19, R.id.imageButton20, R.id.imageButton21, R.id.imageButton22, R.id.imageButton23, R.id.imageButton24, R.id.imageButton25, R.id.imageButton26, R.id.imageButton27, R.id.imageButton28, R.id.imageButton29, R.id.imageButton30}, {"Mui Fa Kuen", "Lau Gar Kuen", "Wu Tip Jeung", "Sap Tok Sau", "Lau Gar Chiang or Jeung", "Fu Pao Kuen", "Kung Chi Fok Fu Kuen", "Fu Hok Seung Ying Kuen", "Sap Ying Kuen", "Yick Sau Kuen", "Tit Shin Kuen", "Hou Chee Kwan Fat", "Lau Gar Kwan", "Pack Qua Dan Dou", "Han Yu Shan Dou", "Wu Dip Che Mo Dou", "Ng Moon Mui Fa Ying Cheong", "Ng Long Bai Qua Kwan Fat", "Yue May Kwan Fat", "Sheung Bai Shou", "Quan Lun Gim Fat", "Yew Gar Tai Pa", "Kwan Do/Chuen Chow Tsi Dou", "Kung Chi Fok Fu Kuen Deui Chuck", "Fu Hok Seung Ying Kuen Deui Chuck", "Hou Chee Kwan Deui Chuck", "Ng Long Bai Qua Kwan Deui Chuck", "Shan Dou Toe Ying Cheong", "Sheung Bai Sau Toe Ying Cheong", "Dan Dou Toe Ying Cheong"}};
    ArrayList<SmallImageButton> obrazki = new ArrayList<SmallImageButton>();
    int firstRowFlag = 0;
    TableRow row = null;
    ImageButton upImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);



    }

    @Override
    public void onResume() {
        super.onResume();


        for (int i = 0; i < buttons[0].length; i++) {
            ImageButton downImageButton = (ImageButton) findViewById(Integer.valueOf(String.valueOf(buttons[0][i])));
            downImageButton.setOnClickListener(addUpImageButtonFromDownImageButton);
        }




    }

    public View.OnClickListener addUpImageButtonFromDownImageButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int flagaA=0;
            for (int i = 0; i < buttons[0].length; i++) {
                if (Integer.valueOf(String.valueOf(buttons[0][i])) == v.getId()) {
                    String wynik = ""+buttons[1][i];
                    TableLayout layout = (TableLayout) findViewById(R.id.tab1);
                    if (firstRowFlag == 0) {
                        row = new TableRow(getApplicationContext());
                        row.setLayoutParams(new TableRow.LayoutParams(
                                TableRow.LayoutParams.MATCH_PARENT,
                                TableRow.LayoutParams.MATCH_PARENT));
                        layout.addView(row);
                        firstRowFlag++;

                        SmallImageButton button = new SmallImageButton(getApplicationContext());
                        button.setBackgroundResource(R.mipmap.ic_100x100);
                        button.setName(wynik);
                        obrazki.add(button);
                        row.addView(button);
                    }
                    if (row.getChildCount()==6) {
                        row = new TableRow(getApplicationContext());
                        row.setLayoutParams(new TableRow.LayoutParams(
                                TableRow.LayoutParams.MATCH_PARENT,
                                TableRow.LayoutParams.MATCH_PARENT));
                        layout.addView(row);
                    }
                    Log.d("Sizee",""+obrazki.size());

                    for (int j = 0; j<obrazki.size(); j++){
                    Log.d("obrazkiobrazkiobrazki",obrazki.get(j).getName());
                    Log.d("wynikwynikwynikwynik",wynik);
                        if ((obrazki.get(j).getName().equals(wynik))){
                            flagaA=1;
                        }

                    }
                    if(flagaA==0){
                        SmallImageButton button = new SmallImageButton(getApplicationContext());
                        button.setBackgroundResource(R.mipmap.ic_100x100);
                        button.setName(wynik);
                        obrazki.add(button);
                        row.addView(button);
                    }
                }
            }

            if(obrazki.size()>0) {
                for (int i = 0; i < obrazki.size(); i++) {
                    ImageButton upImageButton = (ImageButton) obrazki.get(i);
                    upImageButton.setOnClickListener(deleteUpImageButton);
                }
            }
        }

    };

    public View.OnClickListener deleteUpImageButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        Log.d("Poka","Czy robi sens");
            for(Object x : obrazki){
                x.
            }
        }
    };

}
