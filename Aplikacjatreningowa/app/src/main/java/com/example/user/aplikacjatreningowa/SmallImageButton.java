package com.example.user.aplikacjatreningowa;

import android.content.Context;
import android.widget.ImageButton;

/**
 * Created by User on 2016-07-14.
 */
public class SmallImageButton extends ImageButton {
    String name;
    public SmallImageButton(Context context) {
        super(context);
    }
    void setName(String s){
        name = s;
    }
    String getName(){
        return name;
    }
}
