package com.example.hummn.learningworkhouer;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private Button learningButton;
    private Button playingButton;
    private TextView learnedMinuteView;
    private TextView playedMinuteView;
    private Chronometer chronometer;
    private ProgressBar goodBadBar;
    private String learningSharedMinutes = "com.example.hummn.learningworkhouer.learning.minute";
    private String playingSharedMinutes = "com.example.hummn.learningworkhouer.playing.minute";
    boolean playingButtonClicked = false;
    boolean learningButtonClicked = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        playingButton = (Button) findViewById(R.id.playingButton);
        learningButton = (Button) findViewById(R.id.learningButton);
        learnedMinuteView = (TextView) findViewById(R.id.learnedMinuteView);
        playedMinuteView = (TextView) findViewById(R.id.playedMinuteView);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        goodBadBar = (ProgressBar) findViewById(R.id.goodBadBar);

        long learningMinuteFromShared = readValueFromShared(learningSharedMinutes);
        learnedMinuteView.setText(Long.toString(learningMinuteFromShared));
        long playingMinuteFromShared = readValueFromShared(playingSharedMinutes);
        playedMinuteView.setText(Long.toString(playingMinuteFromShared));
        setGoodBadBar();

        playingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!learningButtonClicked) {
                    if(playingButtonClicked){
                        playingButton.setBackgroundColor(Color.LTGRAY);
                        playingButtonClicked = false;
                    } else {
                        playingButton.setBackgroundColor(Color.GRAY);
                        playingButtonClicked = true;
                    }
                    clickRepercussions(playingSharedMinutes, playedMinuteView);
                }
            }
        });


        learningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!playingButtonClicked){
                    if(learningButtonClicked){
                        learningButton.setBackgroundColor(Color.LTGRAY);
                        learningButtonClicked = false;
                    } else {
                        learningButton.setBackgroundColor(Color.GRAY);
                        learningButtonClicked = true;
                    }
                    clickRepercussions(learningSharedMinutes,learnedMinuteView);
                }
            }
        });
    }

    void writeValueToShared(String minuteSharedPreferencesName, long minutes) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(minuteSharedPreferencesName, minutes);
        editor.apply();
    }

    long readValueFromShared(String minuteSharedPreferencesName) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        long numberOfHour = preferences.getLong(minuteSharedPreferencesName, 0);
        return numberOfHour;
    }

    long chronometerButtonActrion() {
        long minutes = 0;
        if (chronometer.getVisibility() == View.VISIBLE) {
            chronometer.setVisibility(View.INVISIBLE);
            chronometer.stop();
            minutes = ((SystemClock.elapsedRealtime()-chronometer.getBase())/1000)/60;
        } else {
            chronometer.setVisibility(View.VISIBLE);
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();
        }
        return minutes;
    }

    void clickRepercussions(String sharedMinutesVariable, TextView componentView) {
        long minute = chronometerButtonActrion();
        minute += readValueFromShared(sharedMinutesVariable);
        writeValueToShared(sharedMinutesVariable, minute);
        componentView.setText(Long.toString(minute));
        setGoodBadBar();
    }

    void setGoodBadBar(){
        long learnedMinuteViewLong = Long.valueOf(String.valueOf(learnedMinuteView.getText()));
        long playedMinuteViewLong = Long.valueOf(String.valueOf(playedMinuteView.getText()));
        goodBadBar.setMax((int) (learnedMinuteViewLong + playedMinuteViewLong));
        goodBadBar.setProgress((int) learnedMinuteViewLong);

    }
}
